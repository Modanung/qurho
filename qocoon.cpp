/* QUrho
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/


#include "weaver.h"

#include "qocoon.h"

Qocoon::Qocoon(Context* context) : QMainWindow(nullptr, Qt::Window | Qt::WindowStaysOnTopHint | Qt::X11BypassWindowManagerHint), Object(context),
    centralView_{ nullptr },
    closing_{ false }
{
    setWindowTitle(Weaver::applicationName());
    setWindowIcon(QIcon("Resources/icon.png"));

    GetSubsystem<Weaver>()->createScene();

    centralView_ = new UrhoWidget(context_);
    centralView_->setScene(GetSubsystem<Weaver>()->getScene());
    setCentralWidget(centralView_);
    context_->RegisterSubsystem(centralView_);

    setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::FramelessWindowHint);
    setStyleSheet("background:transparent;");

    setFixedSize(800, 800);
    show();
}

void Qocoon::updateView()
{
    centralView_->updateView();
}

QPoint Qocoon::cursorDelta()
{
    return mapFromGlobal(QCursor::pos()) - QPoint(width() / 2, height() / 2);
}

void Qocoon::mousePressEvent(QMouseEvent*)
{
    closing_ = true;
}
