/* QUrho
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QWidget>
#include <QTimer>
#include <QDebug>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include "hero.h"
#include "qocoon.h"

#include "weaver.h"

Weaver::Weaver(Context* context, int & argc, char** argv) : QApplication(argc, argv), Application(context),
    requireUpdate_{ true },
    mainWindow_{ nullptr },
    scene_{ nullptr }
{
    QCoreApplication::setOrganizationName("Urho3D");
    QCoreApplication::setOrganizationDomain("urho3d.github.io");
    QCoreApplication::setApplicationName("Urho");

}
Weaver::~Weaver()
{
    delete mainWindow_;
}

void Weaver::Setup()
{
    QWidget* fishTrap{ new QWidget() };

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("urho3d", "logs") +
                                        toString(applicationName().toLower()) + ".log";

    engineParameters_[EP_RESOURCE_PATHS] = "Resources;";
    engineParameters_[EP_EXTERNAL_WINDOW] = (void*)(fishTrap->winId());
    engineParameters_[EP_WORKER_THREADS] = false;

    delete fishTrap;
}

void Weaver::Start()
{
    SetRandomSeed(GetSubsystem<Time>()->GetSystemTime());
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    cache->SetAutoReloadResources(true);
    context_->RegisterSubsystem(this);

    context_->RegisterFactory<Hero>();

    mainWindow_ = new Qocoon(context_);

    QTimer timer;
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer.start(17);

    exec();
    exit();
}
void Weaver::onTimeout()
{
    if (requireUpdate_ && !mainWindow_->isMinimized()
            && engine_ && !engine_->IsExiting()) {

        runFrame();
    }
}
void Weaver::runFrame()
{
    requireUpdate_ = false;

    engine_->RunFrame();

    mainWindow_->updateView();

//    qDebug() << GetSubsystem<Time>()->GetTimeStamp().CString() << " Ran frame";
}
void Weaver::Stop()
{
    engine_->DumpResources(true);
}
void Weaver::exit()
{
    engine_->Exit();
}

void Weaver::createScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    Zone* zone{ scene_->CreateComponent<Zone>() };
    zone->SetAmbientColor(Color(0.055f, 0.17f, 0.23f));
    zone->SetBoundingBox(BoundingBox(Vector3::ONE * -23.0f, Vector3::ONE * 23.0f));

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(-2.0f, 3.0f, -0.23f));
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetBrightness(1.23f);
    light->SetDrawDistance(23.0f);
    light->SetRadius(23.0f);

    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3(0.0f, 0.0f, -12.0f));
    cameraNode->LookAt(Vector3::ZERO);
    cameraNode->CreateComponent<Camera>()->SetFov(23.0f);
    //Urho!
    scene_->CreateChild("Urho")->CreateComponent<Hero>();
}
