/* QUrho
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/


#ifndef WEAVER_H
#define WEAVER_H

#include "urho3d.h"
#include <QApplication>

#include <QPixmap>
#include <QPainter>

#define CONFIGURATIONFILENAME "QUrho.conf"

class Qocoon;

class Weaver : public QApplication,  public Application
{
    Q_OBJECT
    URHO3D_OBJECT(Weaver, Application)
public:
    Weaver(Context* context, int & argc, char** argv);
    virtual ~Weaver();


    void createScene();

    void Setup() override;
    void Start() override;
    void Stop()  override;
    void exit();
    void runFrame();

    Scene* getScene() const { return scene_; }
    Qocoon* mainWindow() const { return mainWindow_; }

public slots:
    void onTimeout();
    void requestUpdate() { requireUpdate_ = true; }

private:
    bool requireUpdate_;
    Qocoon* mainWindow_;
    Scene* scene_;
};

static QString toQString(String string) {

    return QString{ string.CString() };
}
static String toString(QString qString) {

    return String{ qString.toLatin1().data() };
}
static QColor toQColor(const Color color, bool alpha = true) {

    return QColor::fromRgbF( std::min(std::max(0.0f, color.r_), 1.0f),
                             std::min(std::max(0.0f, color.g_), 1.0f),
                             std::min(std::max(0.0f, color.b_), 1.0f),
                    (alpha ? std::min(std::max(0.0f, color.a_), 1.0f) : 1.0f) );
}
static QPixmap toPixmap(Image* image) {

    return QPixmap::fromImage(QImage{
                  image->GetData(),
                  image->GetWidth(),
                  image->GetHeight(),
                  QImage::Format_RGBA8888 });
}

#endif // WEAVER_H
