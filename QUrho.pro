TARGET = urho

QT += core gui widgets

LIBS += \
    $${PWD}/Urho3D/lib/libUrho3D.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    Urho3D/include \
    Urho3D/include/Urho3D/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

HEADERS += \
    weaver.h \
    qocoon.h \
    urho3d.h \
    urhowidget.h \
    hero.h

SOURCES += \
    main.cpp \
    weaver.cpp \
    qocoon.cpp \
    urhowidget.cpp \
    hero.cpp
