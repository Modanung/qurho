/* QUrho
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QResizeEvent>
#include <QPainter>
#include "weaver.h"
#include "urhowidget.h"

UrhoWidget::UrhoWidget(Context* context, QWidget *parent) : QWidget(parent), Object(context),
    scene_{ nullptr },
    camera_{ nullptr },
    renderTexture_{ nullptr },
    image_{ nullptr },
    previousMousePos_{ width() / 2, height() / 2 }

{
    setObjectName("Urho");
    setMinimumSize(80, 45);
    setFocusPolicy(Qt::StrongFocus);
    setMouseTracking(true);

    pixmap_ = QPixmap(width(), height());
    pixmap_.fill(Qt::transparent);

    fade_ = new QGraphicsOpacityEffect(this);
    setGraphicsEffect(fade_);
}
void UrhoWidget::setScene(Scene* scene)
{
    scene_ = scene;
    camera_ = scene_->GetChildrenWithComponent("Camera", true).Front()->GetComponent<Camera>();

    createRenderTexture();
}
void UrhoWidget::createRenderTexture()
{
    if (renderTexture_)
        renderTexture_->GetRenderSurface()->Release();

    renderTexture_ = new Texture2D(context_);
    renderTexture_->SetSize(width(), height(), Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
    renderTexture_->GetRenderSurface()->SetUpdateMode(SURFACE_MANUALUPDATE);

    updateViewport();
}
void UrhoWidget::updateViewport()
{
    if (!camera_)
        return;

    RenderSurface* renderSurface{ renderTexture_->GetRenderSurface() };

    if (renderSurface->GetViewport(0)) {
        renderSurface->GetViewport(0)->SetScene(scene_);

    } else {

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        SharedPtr<Viewport> viewport{ new Viewport(context_, scene_, camera_ )};
        viewport->GetRenderPath()->Load(cache->GetResource<XMLFile>("RenderPaths/ForwardTransparent.xml"));
        renderSurface->SetViewport(0, viewport);
    }

    if (height())
        camera_->SetZoom(Min(1.0f, static_cast<float>(width()) / height()));

    updateView();
}
void UrhoWidget::updateView(StringHash, VariantMap&)
{
    updateView();
}
void UrhoWidget::updateView()
{
    if (!renderTexture_)
        return;

    renderTexture_->GetRenderSurface()->QueueUpdate();
    GetSubsystem<Weaver>()->requestUpdate();

    SubscribeToEvent(E_ENDRENDERING, URHO3D_HANDLER(UrhoWidget, paintView));
}


void UrhoWidget::resizeEvent(QResizeEvent*)
{
    if (!GetSubsystem<Weaver>())
        return;

    createRenderTexture();
}
void UrhoWidget::paintEvent(QPaintEvent*)
{
    if (!camera_)
        return;

    paintView();
}
void UrhoWidget::paintView(const StringHash, VariantMap&)
{
    repaint();
}
void UrhoWidget::paintView()
{
    if (!camera_)
        return;

    updatePixmap();


    int drawWidth{ static_cast<int>(ceil(pixmap_.width() * (static_cast<float>(height()) / pixmap_.height()))) };
    int drawHeight{ static_cast<int>(ceil(pixmap_.height() * (static_cast<float>(width()) / pixmap_.width()))) };
    int dX{ width() - drawWidth };
    int dY{ height() - drawHeight };
    QPainter p(this);

    if (dX > 0 || dY > 0) {

        p.fillRect(rect(), toQColor(GetSubsystem<Renderer>()->GetDefaultZone()->GetFogColor(), false));
    }

    p.drawPixmap(QRect(dX / 2, dY / 2, drawWidth, drawHeight), pixmap_);
}
void UrhoWidget::updatePixmap()
{
    Image* image{ renderTexture_->GetImage() };

    if (!image || renderTexture_->GetRenderSurface()->IsUpdateQueued())
        return;

    if (image_ != image) {

        image_  = renderTexture_->GetImage();
        pixmap_ = toPixmap(image_);

        UnsubscribeFromEvent(E_ENDRENDERING);
    }
}

void UrhoWidget::mouseMoveEvent(QMouseEvent *event)
{return;

    setFocus();

    QPoint dPos{ QCursor::pos() - previousMousePos_ };

    Vector2 dVec{ dPos.x() * 0.00125f, dPos.y() * 0.001666f };

    if (QApplication::mouseButtons()) {

        Node* modelNode{ scene_->GetChild("Urho") };
        if (!modelNode)
            return;

        float pitchDelta{ dVec.y_ * -235.0f };
        modelNode->Rotate(Quaternion(dVec.x_ * -235.0f, Vector3::UP) *
                          Quaternion(pitchDelta, camera_->GetNode()->GetRight()), TS_WORLD);

        //Wrap cursor
        if (event->pos().x() <= 0)
            QCursor::setPos(QCursor::pos() + QPoint(width() - 2, 0));

        else if (event->pos().x() >= width())
            QCursor::setPos(QCursor::pos() - QPoint(width() - 2, 0));

        else if (event->pos().y() <= 0)
            QCursor::setPos(QCursor::pos() + QPoint(0, height() - 2));

        else if (event->pos().y() >= height())
            QCursor::setPos(QCursor::pos() - QPoint(0, height() - 2));

        updateView();
    } else {

//        cursor->HandleMouseMove();
    }

    previousMousePos_ = QCursor::pos();
}

void UrhoWidget::setOpacity(double opacity)
{
    fade_->setOpacity(opacity);
}
