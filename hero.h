/* QUrho
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef HERO_H
#define HERO_H

#include "urho3d.h"

class Hero : public LogicComponent
{
    URHO3D_OBJECT(Hero, LogicComponent)
public:
    Hero(Context* context);
    void OnNodeSet(Node* node) override;
    void Update(float timeStep) override;

    float Depth() const { return node_->GetWorldPosition().z_;}

private:
    AnimationController* animControl_;
    Vector3 targetDirection_;
    Vector3 direction_;
    float velocity_;
    void AdjustHead(StringHash, VariantMap&);
};

#endif // HERO_H
