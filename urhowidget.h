/* QUrho
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef VIEW3DWIDGET_H
#define VIEW3DWIDGET_H

#include <QGraphicsOpacityEffect>

#include <QWidget>
#include "urho3d.h"

class UrhoWidget : public QWidget, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(UrhoWidget, Object)
public:
    explicit UrhoWidget(Context* context, QWidget *parent = nullptr);

    void updateView();
    void setScene(Scene* scene);
    void setOpacity(double opacity);

protected:
    void resizeEvent(QResizeEvent*) override;
    void paintEvent(QPaintEvent*) override;
    void mouseMoveEvent(QMouseEvent* event) override;

private:
    void createRenderTexture();

    void updateViewport();
    void updatePixmap();
    void updateView(StringHash, VariantMap&);
    void paintView();
    void paintView(const StringHash, VariantMap&);

    Scene*               scene_;
    Camera*              camera_;
    SharedPtr<Texture2D> renderTexture_;
    SharedPtr<Image>     image_;
    QPixmap              pixmap_;

    QPoint previousMousePos_;
    QGraphicsOpacityEffect* fade_;
};

#endif // VIEW3DWIDGET_H
