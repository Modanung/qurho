/* QUrho
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QDesktopWidget>
#include "urhowidget.h"
#include "weaver.h"
#include "qocoon.h"
#include "hero.h"

Hero::Hero(Context* context) : LogicComponent(context),
    animControl_{ nullptr },
    targetDirection_{   Vector3::BACK },
    direction_{         Vector3::BACK },
    velocity_{ 0.0f }
{
}

void Hero::OnNodeSet(Node* node)
{
    if (!node)
        return;

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    Model* urhoModel{ cache->GetResource<Model>("Models/Urho.mdl") };
    node_->SetPosition(Vector3::FORWARD * 23.0f);
    node_->LookAt(direction_);
    AnimatedModel* urho{ node_->CreateComponent<AnimatedModel>() };
    urho->SetModel(urhoModel);
    urho->SetMaterial(cache->GetResource<Material>("Materials/Urho.xml"));
    animControl_ = node_->CreateComponent<AnimationController>();
    animControl_->PlayExclusive("Models/Swim.ani", 0, true);
    animControl_->SetSpeed("Models/Swim.ani", 0.666f);

    SubscribeToEvent(E_SCENEDRAWABLEUPDATEFINISHED, URHO3D_HANDLER(Hero, AdjustHead));
}


void Hero::Update(float timeStep)
{
    double fadeStart{ 17.0 };
    double fadeEnd{ 23.0 };
    double fadeLength{ fadeEnd - fadeStart };

    velocity_ = Depth() + 4.2f;
    animControl_->SetSpeed("Models/Swim.ani", 0.17f * velocity_ + 0.23f);

    Quaternion newRotation{};
    newRotation.FromLookRotation(direction_);
    node_->SetRotation(node_->GetRotation().Slerp(newRotation, timeStep * velocity_));
    node_->Translate(Vector3::FORWARD * direction_ * velocity_ * timeStep, TS_WORLD);

    Qocoon* qocoon{ GetSubsystem<Weaver>()->mainWindow() };
    QPoint cursorDelta{ qocoon->cursorDelta() };
    QRect screenSize{ QApplication::desktop()->screenGeometry() };

    if (Depth() < 2.3f && cursorDelta.manhattanLength() < 55) {

        while((QCursor::pos() - (qocoon->pos() + QPoint(qocoon->width(), qocoon->height()) / 2)).manhattanLength() < 235)
            QCursor::setPos(QPoint(random() % screenSize.width(),
                                   random() % screenSize.height()));
    }

    targetDirection_ =  Vector3(Sign(cursorDelta.x()) * pow(cursorDelta.x() * 0.017f, 2.0f),
                -Sign(cursorDelta.y()) * pow(cursorDelta.y() * 0.017f, 2.0f),
                (cursorDelta.manhattanLength() * 0.042f + 0.23f) - Depth()).Normalized();

    direction_ = direction_.Lerp(qocoon->closing() ? Vector3::FORWARD : targetDirection_,
                                 timeStep * sqrt(1.0f + direction_.Angle(targetDirection_) * 0.005f) / (1.0f + direction_.Angle(targetDirection_) * 0.005f));

    if (Depth() > fadeEnd) {

        qocoon->close();
    }

    QPoint windowDelta{ static_cast<int>(round(direction_.x_ * velocity_)),
                        static_cast<int>(round(-direction_.y_ * velocity_)) };
    qocoon->move(qocoon->pos() + windowDelta);

    GetSubsystem<UrhoWidget>()->setOpacity(std::max(0.0, std::min(fadeLength, fadeEnd - Depth()) / fadeLength));
}

void Hero::AdjustHead(StringHash, VariantMap&)
{
    Node* headNode{ node_->GetChild("Head", true) };
    headNode->LookAt(headNode->GetWorldPosition() + targetDirection_ + direction_ * 9.0f);
    headNode->Rotate(Quaternion(90.0f, Vector3::RIGHT));
}
